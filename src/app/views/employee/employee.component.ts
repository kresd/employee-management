import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, Inject, LOCALE_ID, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Employee, Employees } from 'src/app/models/employee';

export interface DialogData {
  id: string;
}

// Call dummy data
const ELEMENT_DATA: Employee[] = Employees;

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})

export class EmployeeComponent implements AfterViewInit {

  // Init
  displayedColumns: string[] = ['username', 'firstName', 'lastName', 'email', 'birthDate', 'basicSalary', 'status', 'group', 'id'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  filterForm: FormGroup;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(public dialog: MatDialog, private fb: FormBuilder, private _router: Router, private datePipe: DatePipe, @Inject(LOCALE_ID) public locale: string) {
    this.dataSource.filterPredicate = ((data, filter) => {
      const a = !filter.username || data.username.toLowerCase().includes(filter.username);
      const b = !filter.status || data.status.toLowerCase().includes(filter.status);
      return a && b;
    }) as (Employee: any, string: any) => boolean;

    this.filterForm = fb.group({
      username: '',
      status: '',
    })
    
    this.filterForm.valueChanges.subscribe(value => {
      const filter = { ...value, username: value.username.trim().toLowerCase() } as string;
      this.dataSource.filter = filter;
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // Navogate to Page Update Employee
  updateEmployee(id: any) {
    this._router.navigate(['/admin/employee/update/' + id]);
  }
  // Call Delete Modal
  openDialogDelete(id: any) {
    this.dialog.open(DeleteEmployeeDialog, {
      data: { id: id }
    });
  }

  // Call Detail Modal
  openDialogDetail(id: any) {
    let datax = ELEMENT_DATA;
    datax = datax.filter(i => id.includes(i.id));

    let split = datax[0].birthDate.split("/", 3);
    let birthDate_ = new Date(split[2] + '-' + split[1] + '-' + split[0]);

    let split_ = datax[0].description.split("/", 3);
    let description_ = new Date(split_[2] + '-' + split_[1] + '-' + split_[0]);

    this.dialog.open(DetailEmployeeDialog, {
      data: {
        id: datax[0].id,
        username: datax[0].username,
        firstName: datax[0].firstName,
        lastName: datax[0].lastName,
        email: datax[0].email,
        birthDate: this.datePipe.transform(birthDate_, "dd-MM-yyyy"),
        basicSalary: 'Rp ' + this.convertRp(datax[0].basicSalary),
        status: datax[0].status,
        group: datax[0].group,
        description: this.datePipe.transform(description_, "dd-MM-yyyy")
      },
      height: 'auto',
      width: '700px',
    });
  }

  // Convert to Rp
  convertRp(numb: any) {
    const format = numb.split('').reverse().join('');
    let convert = format.match(/\d{1,3}/g);
    const rupiah = convert.join('.').split('').reverse().join('') + ',00';
    return rupiah;
  }
}

// Delete Modal
@Component({
  templateUrl: '../dialog/delete-employee-dialog.component.html',
})

export class DeleteEmployeeDialog {

  constructor(
    public dialogRef: MatDialogRef<DeleteEmployeeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private _snackBar: MatSnackBar
  ) { }

  closed(): void {
    this.dialogRef.close();
  }

  deleteEmployee(id: any) {
    this.dialogRef.close();
    this.openSnackBar('Data has been Removed', 'Close');
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top',
      panelClass: ['warning']
    });
  }
}

// Detail Modal
@Component({
  templateUrl: '../dialog/detail-employee-dialog.component.html',
})

export class DetailEmployeeDialog {

  constructor(
    public dialogRef: MatDialogRef<DetailEmployeeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Employee
  ) { }

  closed(): void {
    this.dialogRef.close();
  }
}