import { formatDate } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ReplaySubject, Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { Group, Groups } from 'src/app/models/demo-data';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-employee-add',
  templateUrl: './employee-add.component.html',
  styleUrls: ['./employee.component.scss']
})

export class EmployeeAddComponent implements OnInit, AfterViewInit, OnDestroy {

  // Init Select Search
  protected groupl: Group[] = Groups;
  public groupCtrl: FormControl = new FormControl('', [
    Validators.required
  ]);
  public groupFilterCtrl: FormControl = new FormControl();
  public filteredGroup: ReplaySubject<Group[]> = new ReplaySubject<Group[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect!: MatSelect;
  protected _onDestroy = new Subject<void>();

  // Init Form
  submitted = false;
  maxDate!: Date;

  addEmployeeForm!: FormGroup;
  username = new FormControl('', [
    Validators.required
  ]);

  email = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  firstName = new FormControl('', [
    Validators.required
  ]);

  lastName = new FormControl('', [
    Validators.required
  ]);

  basicSalary = new FormControl('', [
    Validators.required
  ]);

  birthDate = new FormControl('', [
    Validators.required
  ]);

  status = new FormControl('', [
    Validators.required
  ]);

  description = new FormControl('', [
    Validators.required
  ]);

  constructor(private fb: FormBuilder, private _router: Router, private _snackBar: MatSnackBar) {
    this.maxDate = new Date();
  }

  // Call form validation
  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {

    // set initial selection
    this.groupCtrl.setValue(this.groupl[10]);

    // load the initial group list
    this.filteredGroup.next(this.groupl.slice());

    // listen for search field value changes
    this.groupFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroup();
      });

    // Form Add Employee
    this.addEmployeeForm = this.fb.group({
      id: '',
      username: this.username,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      birthDate: this.birthDate,
      basicSalary: this.basicSalary,
      status: this.status,
      group: this.groupCtrl,
      description: this.description,
    });
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected setInitialValue() {
    this.filteredGroup
      .pipe(take(1), takeUntil(this._onDestroy))
      .subscribe(() => {
        this.singleSelect.compareWith = (a: Group, b: Group) => a && b && a.id === b.id;
      });
  }

  protected filterGroup() {
    if (!this.groupl) {
      return;
    }
    // get the search keyword
    let search = this.groupFilterCtrl.value;
    if (!search) {
      this.filteredGroup.next(this.groupl.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredGroup.next(
      this.groupl.filter(group => group.name.toLowerCase().indexOf(search) > -1)
    );
  }

  // // Form Add Employee
  get f() { return this.addEmployeeForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.addEmployeeForm.invalid) {
      return;
    }

    let patchValue = ({
      username: this.addEmployeeForm.value.username,
      firstName: this.addEmployeeForm.value.firstName,
      lastName: this.addEmployeeForm.value.lastName,
      email: this.addEmployeeForm.value.email,
      birthDate: formatDate(this.addEmployeeForm.value.birthDate, 'yyyy-mm-dd', 'en-US'),
      basicSalary: this.addEmployeeForm.value.basicSalary,
      status: this.addEmployeeForm.value.status,
      group: this.addEmployeeForm.value.group,
      description: this.addEmployeeForm.value.description,
    });

    // Debug raw body add employe
    console.log(patchValue);

    this._router.navigate(['/admin/employee']).then((navigated: boolean) => {
      if (navigated) {
        this.openSnackBar('Data Has Been Saved Successfully', 'Close', 9999999);
      }
    });

  }

  // Call Snackbar
  openSnackBar(message: string, action: string, durations: number) {
    this._snackBar.open(message, action, {
      duration: durations,
      verticalPosition: 'top',
      panelClass: ['warning']
    });
  }
}