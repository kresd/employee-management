import { formatDate } from '@angular/common';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ReplaySubject, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Group, Groups } from 'src/app/models/demo-data';
import { Employee, Employees } from 'src/app/models/employee';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

const ELEMENT_DATA: Employee[] = Employees;

@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html'
})
export class EmployeeUpdateComponent implements OnInit, OnDestroy {

  // Init Select Search
  protected groupl: Group[] = Groups;

  public groupCtrl: FormControl = new FormControl('', [
    Validators.required
  ]);

  public groupFilterCtrl: FormControl = new FormControl();

  public filteredGroup: ReplaySubject<Group[]> = new ReplaySubject<Group[]>(1);

  @ViewChild('singleSelect', { static: true }) singleSelect!: MatSelect;

  protected _onDestroy = new Subject<void>();

  // Init Form
  subcribeTopic!: Subscription;
  submitted = false;
  maxDate!: Date;
  editEmployeeForm!: FormGroup;
  username = new FormControl('', [
    Validators.required
  ]);

  email = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  firstName = new FormControl('', [
    Validators.required
  ]);

  lastName = new FormControl('', [
    Validators.required
  ]);

  basicSalary = new FormControl('', [
    Validators.required
  ]);

  birthDate = new FormControl('', [
    Validators.required
  ]);

  status = new FormControl('', [
    Validators.required
  ]);

  description = new FormControl('', [
    Validators.required
  ]);

  constructor(private fb: FormBuilder, private _activeRoute: ActivatedRoute, private _router: Router, private _snackBar: MatSnackBar) {
    this.maxDate = new Date();
  }

  matcher = new MyErrorStateMatcher();

  ngOnInit(): void {
    // set initial selection
    this.groupCtrl.setValue(this.groupl[10]);

    // load the initial group list
    this.filteredGroup.next(this.groupl.slice());

    // listen for search field value changes
    this.groupFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGroup();
      });

    //Form EdIT Employee
    this.editEmployeeForm = this.fb.group({
      id: '',
      username: this.username,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
      birthDate: this.birthDate,
      basicSalary: this.basicSalary,
      status: this.status,
      group: this.groupCtrl,
      description: this.description,
    });

    // Set Value in form
    this.subcribeTopic = this._activeRoute.params.subscribe((params: Params) => {
      const id = params['id'];

      let data = ELEMENT_DATA;
      data = data.filter(i => id.includes(i.id));

      let split = data[0].birthDate.split("/", 3);
      let birthDate_ = new Date(split[2] + '-' + split[1] + '-' + split[0]);

      let split_ = data[0].description.split("/", 3);
      let description_ = new Date(split_[2] + '-' + split_[1] + '-' + split_[0]);

      this.editEmployeeForm.setValue({
        id: data[0].id,
        username: data[0].username,
        firstName: data[0].firstName,
        lastName: data[0].lastName,
        email: data[0].email,
        birthDate: birthDate_,
        basicSalary: data[0].basicSalary,
        status: data[0].status,
        group: data[0].group,
        description: description_,
      });
    }, error => {
      this.openSnackBar('Can\'t Get Data Of Employee', 'Close', 9999999);
    });
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  protected filterGroup() {
    if (!this.groupl) {
      return;
    }
    // get the search keyword
    let search = this.groupFilterCtrl.value;
    if (!search) {
      this.filteredGroup.next(this.groupl.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredGroup.next(
      this.groupl.filter(group => group.name.toLowerCase().indexOf(search) > -1)
    );
  }

  get f() { return this.editEmployeeForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.editEmployeeForm.invalid) {
      return;
    }

    let patchValue = ({
      username: this.editEmployeeForm.value.username,
      firstName: this.editEmployeeForm.value.firstName,
      lastName: this.editEmployeeForm.value.lastName,
      email: this.editEmployeeForm.value.email,
      birthDate: formatDate(this.editEmployeeForm.value.birthDate, 'yyyy-mm-dd', 'en-US'),
      basicSalary: this.editEmployeeForm.value.basicSalary,
      status: this.editEmployeeForm.value.status,
      group: this.editEmployeeForm.value.group,
      description: this.editEmployeeForm.value.description,
    });

    console.log(patchValue);

    this._router.navigate(['/admin/employee']).then((navigated: boolean) => {
      if (navigated) {
        this.openSnackBar('Data Has Been Saved Successfully', 'Close', 9999999);
      }
    });
  }

  openSnackBar(message: string, action: string, durations: number) {
    this._snackBar.open(message, action, {
      duration: durations,
      verticalPosition: 'top',
      panelClass: ['warning']
    });
  }
}