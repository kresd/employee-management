import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { EmployeeAddComponent } from './employee-add.component';
import { EmployeeUpdateComponent } from './employee-update.component';
import { EmployeeComponent } from './employee.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: EmployeeComponent,
  },
  {
    path: 'add',
    canActivate: [AuthGuard],
    component: EmployeeAddComponent
  },
  {
    path: 'update/:id',
    canActivate: [AuthGuard],
    component: EmployeeUpdateComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
