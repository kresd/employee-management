import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {


  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
   
  }

  // Call Logout Modal
  openDialogLogout() {
    this.dialog.open(LogoutDialog);
  }

}


@Component({
  selector: 'logout-dialog',
  templateUrl: '../dialog/logout-dialog.html',
})
export class LogoutDialog {
  constructor(
    public _dialogRef: MatDialogRef<LogoutDialog>,
    private _router: Router,
    private _auth: AuthService) { }

  onNoClick(): void {
    this._dialogRef.close();
  }

  logout(): void {
    this._dialogRef.close();
    this._auth.logout();
    this._router.navigate(['']);
  }
}
