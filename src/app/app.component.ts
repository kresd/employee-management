import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'employee-management';

  constructor(
    private router: Router,
    private auth: AuthService
  ) {
  }

  logout() {
    this.auth.logout();
    this.router.navigate(['/login']);
  }
}
