
export interface Group {
  id: string;
  name: string;
}

export const Groups : Group[] = [
  { name: 'Tech Support', id: '1' },
  { name: 'Administrator', id: '2' },
  { name: 'Marketing', id: '3' },
  { name: 'Keamanan', id: '4' },
  { name: 'Manajer', id: '5' },
  { name: 'Kepala Cabang', id: '6' },
  { name: 'CFO', id: '7' },
  { name: 'HRD', id: '8' },
  { name: 'Kepala Pusat', id: '9' },
  { name: 'Sekretaris', id: '10' },
];